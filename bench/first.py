import sys, os
p = os.path.dirname(os.path.abspath(__file__))
sys.path.append(p)
sys.path.append(os.path.dirname(p))
print (sys.path[-1])

import yter
import timeit
import itertools


data = open(os.path.dirname(p) + "/yter/_fun.py").read()
rng = range(1000, 10000)
itr = itertools.count(2)


t1a = timeit.timeit(lambda: yter.first(data), number=2000)
t1b = timeit.timeit(lambda: yter.first(itertools.count(2)), number=2000)
t2a = timeit.timeit(lambda: next(iter(data)), number=2000)
t2b = timeit.timeit(lambda: next(iter(itertools.count(2))), number=2000)

print("YTER TIME:", t1a, t1b)
print("NEXT TIME:", t2a, t2b)
