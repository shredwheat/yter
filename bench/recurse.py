import os
import sys

import sys, os
p = os.path.dirname(os.path.abspath(__file__))
sys.path.append(p)
sys.path.append(os.path.dirname(p))
print (sys.path[-1])

import yter
import itertools



def recurse(y, call, pred):
    iters = [iter(y)] # Will iterate while modifying this list
    recursion = itertools.chain.from_iterable(iters)
    for item in yter.unique(recursion):
        yield item
        if pred and not pred(item):
            continue
        kids = call(item)
        if kids:
            iters.append(kids)


def main():
    path = "."
    startup = os.scandir(path)
    for ent in recurse(startup, os.scandir, 
                pred=lambda e: e.is_dir() and not e.name.startswith(".")):
        if ent.is_dir():
            pass
        else:
            print(ent.path)


if __name__ == "__main__":
    main()