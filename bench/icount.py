import itertools
import time
import contextlib


def main():
    data = list(range(10000)) * 1000
    size = len(data)

    print("Values:", len(data))

    with test("list", size):
        list(iter(data))

    with test("chain", size):
        list(itertools.chain(data))

    with test("generator", size):
        list(_gen(data))

    with test("genexpr", size):
        list(i for i in data)

    with test("class", size):
        list(_cls(data))


def _gen(i):
    for val in i:
        yield i


class _cls(object):
    def __init__(self, i):
        self.i = i
        it = iter(i)
        if hasattr(it, "__next__"):
            self.n = it.__next__
        else:
            self.n = it.next
    def __iter__(self):
        return self
    def __next__(self):
        #return self.i[self.n()]
        return self.n()
    iter=__iter__
    next=__next__


@contextlib.contextmanager
def test(name, size):
    msize = size / 1000000.0
    s = time.time()
    yield
    e = time.time()
    print("%s: %.03f sec, %.1f mips" % (name, e-s, msize/(e-s)))


if __name__ == "__main__":
    main()
