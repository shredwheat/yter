import sys, os
p = os.path.dirname(os.path.abspath(__file__))
sys.path.append(p)
sys.path.append(os.path.dirname(p))
print (sys.path[-1])

import collections
import itertools
import time
import sys


# Some Python2/3 iterop
try:
    from itertools import zip as izip
except ImportError:
    izip = zip


# 1.0x
def finish(itr):
    """Baseline for loop"""
    count = 0
    for val in itr:
        count += 1
    return count


# 8.49x
def finish2(itr):
    """Empty deque, but cannot know size"""
    collections.deque(itr, maxlen=0)


# 1.72x
def finish3(itr):
    """Zip with itertools.count and empty deque"""
    c = itertools.count()
    x = izip(itr, c)
    collections.deque(x, maxlen=0)
    return next(c)


# 1.23x
def finish4(itr):
    """Enumerate with single deque"""
    e = enumerate(itr, 1)
    d = collections.deque(e, maxlen=1)
    if not d:
        return 0
    return d[0][0]


# 1.41x
def finish5(itr):
    """Copy to a list and call len"""
    return len(list(itr))


# 2.03x
def finish6(itr):
    """Copy to a tuple and call len"""
    return len(tuple(itr))


# 0.82x
def finish8(itr):
    """Sum simple generator"""
    return sum(1 for x in itr)


# 0.54x
def finish9(itr):
    """itertools.count with generator"""
    c = itertools.count()
    if hasattr(c, "__next__"):
        n = c.__next__
    else:
        n = c.next
    collections.deque((n() for x in itr), maxlen=0)
    return next(c)


# 3.56x
import yter
def finishz(itr):
    """yter.finish"""
    return yter.ylen(itr)


def main():
    data = list(range(10000)) * 8000

    if len(sys.argv) >= 2:
        baseline = float(sys.argv[1])
    else:
        baseline = None

    funcs = [v for (k, v) in globals().items() if k.startswith("finish")]
    funcs.sort(key=lambda f: f.__name__)
    for func in funcs:
        value = itertools.chain(data)
        s = time.time()
        result = func(value)
        e = time.time()
        d = e - s
        if baseline is None:
            print("BASELINE TIME: {0:.2f} sec".format(d))
            baseline = d
        print("{0:.02f}X : {1.__doc__} ({1.__name__}) = {2}".format(
            (baseline / d), func, result))



if __name__ == "__main__":
    main()
