import sys, os
p = os.path.dirname(os.path.abspath(__file__))
sys.path.append(p)
sys.path.append(os.path.dirname(p))
print (sys.path[-1])

import yter
import timeit
import collections
import itertools


data = open(os.path.dirname(p) + "/yter/_fun.py").read()
rng = range(1000, 10000)



t1a = timeit.timeit(lambda: yter.last(data), number=2000)
t1b = timeit.timeit(lambda: yter.last(itertools.islice(itertools.count(), 1000, 10000)), number=2000)
t2a = timeit.timeit(lambda: collections.deque(data, 1), number=2000)
t2b = timeit.timeit(lambda: collections.deque(itertools.islice(itertools.count(), 1000, 10000), 1)[0], number=2000)

print("YTER TIME:", t1a, t1b)
print("DEQ TIME:", t2a, t2b)
