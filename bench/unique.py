import sys, os
p = os.path.dirname(os.path.abspath(__file__))
sys.path.append(p)
sys.path.append(os.path.dirname(p))
print (sys.path[-1])

import more_itertools
import yter
import timeit


data = open(os.path.dirname(p) + "/doc/site/index.html").read()

s1 = len(list(yter.unique(data)))
s2 = len(list(more_itertools.unique_everseen(data)))
t1a = timeit.timeit(lambda: list(yter.unique(data)), number=2000)
t1b = timeit.timeit(lambda: list(yter.unique(data, key=ord)), number=2000)
t2a = timeit.timeit(lambda: list(more_itertools.unique_everseen(data)), number=2000)
t2b = timeit.timeit(lambda: list(more_itertools.unique_everseen(data, key=ord)), number=2000)

print("SIZE:", s1, s2)
print("YTER TIME:", t1a, t1b)
print("MORE TIME:", t2a, t2b)
