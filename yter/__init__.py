"""Clever, quick iterators that make your smile whiter."""

from ._fun import *
from ._ytr import *
from ._key import *

__all__ = [
    "yany",
    "yall",
    "ylen",
    "last",
    "head",
    "tail",
    "minmax",
    "isiter",
    "repeatable",
    "uniter",
    "call",
    "percent",
    "flat",
    "chunk",
    "key",
    "choose",
    "unique",
    "duplicates",
    "recurse",
    "format",
    "numeric",
    "getter",
]
