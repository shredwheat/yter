# yter

Version 3.1.1
2023, June 23

Clever, quick iterator functions that make your smile whiter.

This will work with versions of Python 2.6+ and 3.2+.


## Functions

There are many functions that process data from iterators in efficient ways.

* [`yany`](#yany) &mdash;  Extended version of the builtin any, test if any values are true
* [`yall`](#yall) &mdash;  Extended version of the builtin all, test if all values are true
* [`ylen`](#ylen) &mdash;  Complete an iterator and get number of values
* [`first`](#first) &mdash;  Get the first value from an iteraterable
* [`last`](#last) &mdash;  Get the final value from an iteraterable
* [`head`](#head) &mdash;  Get the first values from an iteraterable
* [`tail`](#tail) &mdash;  Get the last values from an iteraterable
* [`minmax`](#minmax) &mdash;  Find the minimum and maximum values from an iterable
* [`isiter`](#isiter) &mdash;  Test if an object is iterable, but not a string type
* [`uniter`](#uniter) &mdash;  Efficient copy of non sequence data
* [`repeat`](#repeat) &mdash;  Efficient lazy copy of non sequence data


## Iterators

There are several iterators that wrap an existing iterator and process it's output.

* [`call`](#call) &mdash;  Iterator that works with mixed callable types
* [`percent`](#percent) &mdash;  Iterator that skips a percentage of values
* [`flat`](#flat) &mdash;  Iterator of values from a iterable of iterators
* [`chunk`](#chunk) &mdash;  Iterator of lists with a fixed size from iterable
* [`key`](#key) &mdash;  Iterator of pairs of key result and original values
* [`choose`](#choose) &mdash;  Split into iterators for true and false values
* [`unique`](#unique) &mdash;  Iterate only the unique values
* [`duplicates`](#duplicates) &mdash;  Iterate only the duplicated values
* [`recurse`](#recurse) &mdash;  Recurse values from a callable


## Keys

Utility functions that are useful to use as a key argument

* [`format`](#format) &mdash;  Create a function that formats given values into strings
* [`numeric`](#numeric) &mdash;  Split a string into string and integer sections
* [`getter`](#getter) &mdash;  Shorthand for attrgetter, itemgetter, and methodcaller operators




#Reference
## yany

    yany(y, key=None, empty=False)


Extended version of the builtin any, test if any values are true.

Unlike the builtin `any` method, this will return the last value iterated
before ending. This will short circuit or exit early when a correct answer
is found.

A simple explanation is that this will be the first true value or the final
false value of the interator. This is the same as calling the logical or
operator on all the iterated values.

The `key` is an optional function that will be called on each value in
iterator. The return of that function will be used to test if the value is
true or false.

If the iterator is empty this will return the empty argument. This defaults
to False, the same as the built in `any`.

### Examples

```python
>>> yter.yany([0, False, 1.1, ""])
1.1
>>> yter.yany([2.2, 3.3, 4.0, 5.5], key=float.is_integer)
4.0
>>> yter.yany([])
False
```

### Implementation details

This will advance the given iterator until the first non-true value
is found, which will not always consume the entire iterator. This will
generally be faster than something like `itertools.takewhile` but
never as fast as the builtin `any` method.


## yall

    yall(y, key=None, empty=True)


Extended version of the builtin all, test if all values are true.

Unlike the builtin `all` method, this will return the last value iterated
before ending. This will short circuit or exit early when a correct answer
is found.

A simple explanation is that this will be the final true value or the first
false value of the interator. This is the same as calling the logical and
operator on all the iterated values.

The `key` is an optional function that will be called on each value in
iterator. The return of that function will be used to test if the value is
true or false.

If the iterable is empty this will return the empty argument. This defaults
to True, the same as the built in `all`.

### Examples

```python
>>> yter.yall([0, False, 1.1, ""])
0
>>> yter.yall([2.0, 3.0, 4.4, 5.0], key=float.is_integer)
4.4
>>> yter.yall([])
True
```

### Implementation details

This will advance the given iterator until the first non-true value
is found, which will not always consume the entire iterator. This will
generally be faster than something like `itertools.takewhile` but
never as fast as the builtin `all` method.


## ylen

    ylen(y)


Complete an iterator and get number of iterations.

Get all the values from an iterator and return the number of values it
contained. An empty iterable will return 0.

The values from the iterator are discarded.

### Examples

```python
>>> yter.ylen("abcdef")
6
>>> yter.ylen(zip("Hello", "Will"))
4
```

### Implementation details

The given iterator will be fully used when complete. If the given object
implements the `len` builtin, it will simply use that. Otherwise this
will advance the iterator in increments of about 4000 items while tallying
the number of items.


## last

    last(y, empty=None)


Get the final value from an iterator.

This will get the final value from an iterable object. If the iterable
contains no values then the `empty` argument is returned.

### Examples

```python
>>> yter.last("abcdef")
'f'
>>> yter.last(range(10_000_000))
9999999
```

### Implementation details

This will check if the builtin `reversed` function can be used. If so it will
get the first value from the iterator. Otherwise the iterable will be
passed through a single item deque.


## head

    head(y, count, container=<class 'list'>)


Get the first values from an iterator.

This will be a list of values no larger than `count`. This is similar to
`yter.first` but returns a container of values, instead of just the first.

The container argument defaults to a list, but any type of container
can be used, that accepts an iterable argument.

### Examples

```python
>>> yter.head(range(10_000_000), 3)
[0, 1, 2]
>>> yter.head("abcdef", 3, container=tuple)
('a', 'b', 'c')
```

### Implementation details

The iterator will always be advanced by the given ``count``.


## tail

    tail(y, count, container=<class 'list'>)


Get the last values from an iterator.

This will be a new list of values no larger than `count`.

If the iterable object can be `reversed` then a more efficient algorithm is
used. Otherwise this will always finish the iterable object.

The container argument defaults to a list, but any type of container
can be used, that accepts an iterable argument.

### Examples

```python
>>> yter.tail(range(10_000_000), 3)
[9999997, 9999998, 9999999]
>>> yter.tail("abcdef", 3, container=tuple)
('d', 'e', 'f')
```

### Implementation details

If the iterator or container supports the `reversed` builtin then this
will only get the first values from that list. Otherwise the iterator
will be fully advanced to get all the values.


## minmax

    minmax(y, key=None, empty=None)


Find the minimum and maximum values from an iterable.

This will always return a tuple of two values. If the iterable contains no
values it the tuple will contain two values of the `empty` argument.

The minimum and maximum preserve order. So the first value that compares
equal will be considered the minimum and the last equal value is considered
the maximum. If you sorted the iterable, this is the same as the first and
last values from that list.

The `key` is an optional function that will be called on each value in
iterator. The return of that function will be used to sort the values from
the iterator.

### Examples

```python
>>> yter.minmax([1, 2, 3, 4, 5])
(1, 5)
>>> yter.minmax(["first", "second", "third], key=len)
('first', 'second')
>>> yter.minmax({}, empty=0.0)
(0.0, 0.0)
```

### Implementation details

This should be faster than calling both `min` and `max` on the same
container. Which is only possible when using containers, not
iterators.


## isiter

    isiter(y, ignore=string_types)


Test if an object is iterable, but not a string type.

Test if an object is an iterator or is iterable itself. By default this does
not return True for string objects. Python 2 will also ignore exception
types.

The `ignore` argument defaults to a list of string types that are not
considered iterable. This can be used to also exclude things like
dictionaries or named tuples. It will be used as an argument to
`isinstance`. If ignore is set to None, it will be ignored.

### Examples

```python
>>> yter.isiter([1, 2, 3])
True
>>> yter.isiter({"a": 1, "b": 2}.items())
True
>>> yter.isiter("abc")
False
>>> yter.isiter(ValueError("Wrong Value"))
False
```


## repeatable

    repeatable(y)


Efficient lazy copy of non sequence data.

If the value is already a sequence, that will be returned with no changes.
Otherwise this will wrap the results in an iterator that lazily copies all
values for repeated use.

This allows efficient use of containers when no copy is needed to iterate
the data multiple times.

This is slightly less efficient than `yter.uniter`, but if only iterating a
portion of the data, this will not require a full copy.

Be aware that repeatable is still an iterable, and calls like len()
will often not work, depending on the iterable argument.

### Examples

```python
>>> iterable = zip("abc", "123")
>>> repeat = yter.repeatable(iterable)
>>> tuple(repeat), tuple(repeat)
((('a', '1'), ('b', '2'), ('c', '3')), (('a', '1'), ('b', '2'), ('c', '3')))
```


## uniter

    uniter(y, container=<class 'list'>)


Ensure any iterable is a real container.

If the value is already a container, like a list or tuple, then return the
original value. Otherwise the value must be an iterator that will be copied
into a new container of the given type. If the value already appears
to be a container and not an iterator, then it will be returned unconverted.

This gives an efficient way to call `len` or repeat an iterator without
duplicating data already in a container.

### Examples

```python
>>> yter.uniter([1, 2, 3])
[1, 2, 3]
>>> yter.uniter(range(5, 8))
[5, 6, 7]
>>> yter.uniter("abc", container=tuple)
('a', 'b', 'c')
>>> yter.uniter({"a": 1, "b": 2}.items())
```


## call

    call(y)


Iterator that works with mixed callable types.

Iterate over all the values from the input iterator. If one of the values is
a callable object it will be called and its return will be the value
instead.

This is mostly useful when iterating callables that may by mixed with
predefined values, which can often be ``None`` or other placeholder values.
It is the same as ``v(*args,**kargs) if callable(v) else v for v in y``

The `args` and `kwargs` arguments will be passed to any callable values in
the iterator.

### Examples

```python
>>> callbacks = [len, str.upper, 5.5]
>>> list(yter.call(callbacks, "hello"))
[5, 'HELLO', 5.5]
```


## percent

    percent(y, pct)


Iterator that skips a percentage of values.

The `pct` is a floating point value between 0.0 and 1.0. If the value is
larger than 1.0 this will iterate every value. If this value is less than
or equal to zero this will iterate no values.

As long as the `pct` is greater than 0.0 the first value will always be
iterated.

### Examples

```python
>>> list(yter.percent(range(10), 0.25))
[0, 4, 8]
>>> list(yter.percent(range(10, -0.1)))
[]
>>> list(yter.percent(range(10), 1.5))
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
```


## flat

    flat(y)


Iterator of values from a iterable of iterators.

This removes a level of nesting. When given a list of lists this will
iterate the values from those children lists.

This will invert the results of the `yter.chunk` iterator.

### Examples

```python
>>> list(yter.flat([[1, 2], [3, 4, 5]]))
[1, 2, 3, 4, 5]
>>> list(yter.flat([range(3), range(6, 3, -1)]))
[0, 1, 2, 6, 5, 4]
```

### Implementation notes

This is the same as `itertools.chain.from_iterable` with a more memorable
name.


## chunk

    chunk(y, size)


Iterator of lists with a fixed size from iterable.

Group the values from the iterable into lists up to a given size. The final
list generated can be smaller than the given size.

### Examples

```python
>>> tuple(yter.chunk(range(10), 3))
([0, 1, 2], [3, 4, 5], [6, 7, 8], [9])
>>> tuple(yter.chunk([], 10))
()
```

### Implementation notes

The iterator will be advanced by the given ``size`` each time it
is iterated.


## key

    key(y, key)


Iterator of pairs of key result and original values

Each value will be a tuple with the return result of the `key` function and
the actual value itself. The `key` function will be called with each value
of the input iterator.

This allows passing the generator to functions like `min` and `sorted`, but
you are able to see the value and the result of the key argument.

### Examples

```python
>>> list(yter.key(["one", "two", "three"], str.upper))
[('ONE', 'one'), ('TWO', 'two'), ('THREE', 'three')]
>>> list(yter.key(range(7), lambda x: x % 3))
[(0, 0), (1, 1), (2, 2), (0, 3), (1, 4), (2, 5), (0, 6)]
```


## choose

    choose(y, key=None)


Separate iterators for true and false values.

Creates two iterators that represent value that are false and values that
are true. A key function can be given that will be used to provide the
value's true or false state.

### Examples

```python
>>> yes, no = yter.choose([0.0, 1.0, 2.0, 3.0])
>>> list(yes)
[1.0, 2.0, 3.0]
>>> list(no)
[0.0]
```

### Implementation notes

Internally this uses an `itertools.tee` to allow separate iterators
over the same data. This will only call the key or nonzero boolean
evaluation once per item.


## unique

    unique(y, key=None)


Iterate only the unique values

Only the first time a value is encountered it will be iterated. After that,
values that are the same will be ignored.

If the key is given, then it will be called on each value before comparing
for uniqueness.

### Examples

```python
>>> list(yter.unique([1, 2, 3, 2, 1]))
[1, 2, 3]
>>> list(yter.unique(["red", "RED", "Green"], str.lower))
['red', 'Green']
```

### Implementation notes

This uses a ``set`` to track which objects have already been found.


## duplicates

    duplicates(y, key=None)


Iterate only the duplicated values

Will iterate a value for each time it is duplicated. If the iterator has
three of the same value this will iterate the final two of them.

If the key is given, then it will be called on each value before comparing
for duplicates.

### Examples

```python
>>> list(yter.duplicates([1, 2, 3, 2, 1]))
[2, 1]
>>> list(yter.duplicates(["red", "RED", "Green"], str.lower))
['RED']
```

### Implementation notes

This uses a ``set`` to track which objects have already been found.


## recurse

    recurse(y, call, filter=None)


Recurse values from a callable.

Iterate a breadth first recursion using the given callable and an
initial iterator of values. The callable is expected to return its
own iterable of children or None, while taking an argument of a single
item from the iterators.

This will ensure only unique values are iterated, which avoids typical
infinite recursion problems with recursive data structures.

The `filt` argument is an optional callable that will be called with
each value. Values generated by the callable will only be recursed
when the filter returns True. The filter will also be called on the
initial iterator argument.

### Examples

```python
>>> friends = {
...    "alice": ["bob", "terry"],
...    "bob": ["edward", "dave"],
...    "dave": ["alice", "bob"],
...    "edward": ["dave", "alice"],
...    "gary": ["dave", "bob"],
... }
>>> list(yter.recurse(["alice"], friends.get))
['alice', 'bob', 'terry', 'edward', 'dave']
```

```python
entries = os.scandir(".")
def ispath(e): return e.is_dir() and not e.name.startswith(".")
for entry in yter.recurse(entries, os.scandir, filter=ispath):
   if entry.name.endswith(".py"):
       print(entry.path)
```

### Implementation notes

The callable results are treated as an iterator and will not be
iterated until the caller reaches that data.

This does not use function recursion, which avoids Python's typical
stack limits encountered with deep data structures.


## format

    format(fmt)


Create a function that formats given values into strings.

This is similar to converting each value into a string, but is more
convenient to define a function that will do more advanced formattting.

Additional keywoard arguments can be given that will also be passed to the
format function.

The value passed to the function will be given as the first argument to
format, which is referenced as {0}.

### Examples

```python
>>> pct = yter.format("{:!<5.0%} odds")
>>> list(pct(v) for v in (.25, .5, 1))
['25%!! odds', '50%!! odds', '100%! odds']
```

```python
renamer = yter.format("{0.stem}.{ext}, ext="py")
{p: renamer(p) for p in paths}
```


## numeric

    numeric(value)


Split a string into string and integer sections.

A string will be a tuple containing sections that are strings and integers.
The numeric parts of the string will be converted into integers.
The first value in the result will always be a string. If the value argument
starts with numbers then this will be an empty string. This ensures results
from any ``numeric`` call can be compared with others.

Negative numbers will also be converted if preceded by a single minus sign.
This does not attempt to handle floating point numbers, instead those
will be split into two integers with a single character string in the middle.

### Examples

```python
>>> yter.numeric("one01two2")
['one', 1, 'two', 2, '']
>>> yter.numeric("1one-2two")
['', 1, 'one', -2, 'two']
>>> yter.numeric("one01two2") < yter.numeric("1one-2two")
False
>>> yter.numeric("pi starts with 3.14")
['pi starts with ', 3, '.', 14]
```


## getter

Shorthand for the attrgetter, itemgetter, and methodcaller operators.


The same results can be achieved by using `operator.attrgetter` and
`operator.itemgetter`, but this is more concise and can also be used to
lookup nested data.

By using the special syntax, `getter._(args)` you can also create a
callable. This is similar to the `operator.methodcaller` but it doesn't use
a method name. The underscore attribute can be called to create a callable
lookup, which can still be chained with item and attributes.

If an attribute or item is not found this will result in None instead of an
exception, which is more useful for key functions.

You can lookup multiple items by passing multiple values to the index
operator.

Looking up attributes can only be done with a single value.

### Examples

```python
>>> data = {"name": {"first": "Peter", "last": "Shinners"}, "id": 1234}
>>> key = yter.getter["name"]["first"]
>>> print(key(data))
"Peter"
>>> print(yter.getter.real(1.2))
1.2
>>> data = [bool, int, float, str]
>>> print [yter.getter._("12") for d in data]
[True, 12, 12.0, '12']
```

### Implementation details

This combines the `operator.attrgetter` and `operator.itemgetter` as well
as its own logic for handling callable objects.