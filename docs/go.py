#!/usr/bin/env python

import sys
import os
import re

path = os.path.dirname(os.path.abspath(__file__))
os.chdir(path)
sys.path.append(os.path.dirname(path))

import doc2md
import yter


goodToc = open("../README.md").readlines()[:-1]
goodToc = "".join(goodToc)
goodToc += "\n\n#Reference\n###"
goodToc = re.sub(r"\* `(.+?)` --", r"* [`\1`](#\1) &mdash; ", goodToc)

docs = doc2md.mod2md(yter, "yter", "API", toc=True)
mdDocs = re.sub(r"(.*?)###", goodToc, docs, 1, re.MULTILINE|re.DOTALL)
mdDocs = mdDocs.replace("###", "##")

open("docs/index.md", "w").write(mdDocs)

import markdown
htmlDocs = markdown.markdown(mdDocs, extensions=['fenced_code', 'codehilite'])
htmlDocs = '''<!DOCTYPE html><html><head><link rel="stylesheet" href="code.css"></head><body>
\n''' + htmlDocs + '''\n</body></html>'''
open("docs/index.html", "w").write(htmlDocs)